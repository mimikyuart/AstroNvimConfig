return {
  -- You can also add new plugins here as well:
  -- Add plugins, the lazy syntax
  -- "andweeb/presence.nvim",
  -- {
  --   "ray-x/lsp_signature.nvim",
  --   event = "BufRead",
  --   config = function()
  --     require("lsp_signature").setup()
  --   end,
  -- },
     {
       "f-person/git-blame.nvim",
     },
     -- {
     --   "Exafunction/codeium.vim",
     --   event = "User AstroFile",
     --   config = function()
     --     vim.keymap.set("i", "<C-k>", function() return vim.fn["codeium#Accept"]() end, { expr = true })
     --     vim.keymap.set("i", "<c-;>", function() return vim.fn["codeium#CycleCompletions"](1) end, { expr = true })
     --     vim.keymap.set("i", "<c-,>", function() return vim.fn["codeium#CycleCompletions"](-1) end, { expr = true })
     --     vim.keymap.set("i", "<c-x>", function() return vim.fn["codeium#Clear"]() end, { expr = true })
     --     vim.keymap.set("n", "<leader>;", function()
     --       if vim.g.codeium_enabled == true then
     --         vim.cmd "CodeiumDisable"
     --       else
     --         vim.cmd "CodeiumEnable"
     --       end
     --     end, { noremap = true, desc = "Toggle Codeium active" })
     --   end,
     -- },
     {
       "kawre/leetcode.nvim",
       build = ":TSUpdate html",
       dependencies = {
           "nvim-telescope/telescope.nvim",
           "nvim-lua/plenary.nvim", -- required by telescope
           "MunifTanjim/nui.nvim",
 
           -- optional
           "nvim-treesitter/nvim-treesitter",
           "rcarriga/nvim-notify",
           "nvim-tree/nvim-web-devicons",
       },
       lazy = "leetcode.nvim" ~= vim.fn.argv()[1],
       opts = {
          -- configuration goes here
          ---@type string
          arg = "leetcode.nvim",

          ---@type lc.lang
          lang = "cpp",

          -- -@type string
          directory = vim.fn.expand('$HOME/Developer/project/leetcode_notebook')
       },
     },

}
